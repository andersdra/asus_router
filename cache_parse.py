#!/usr/bin/python3
# loads '/tmp/nmp_cache.js' from RT-AC68U, spits out info for MAC address + key value
'''
{'type': '0', 'defaultType': '0', 'name': 'hostname_if_avail_else_vendor', 'nickName': 'custom_in_admin_panel', 'ip': 'xx.x.xx.xx',
 'mac': 'C0:63:FF:CA:F4:BA', 'from': 'networkmapd', 'macRepeat': '1', 'isGateway': '0', 'isWebServer': '0',
 'isPrinter': '0', 'isITunes': '0', 'dpiType': '', 'dpiDevice': '', 'vendor': 'ASUS', 'isWL': '0',
 'isOnline': '1', 'ssid': '', 'isLogin': '0', 'opMode': '0', 'rssi': '0', 'curTx': '', 'curRx': '',
 'totalTx': '', 'totalRx': '', 'wlConnectTime': '', 'ipMethod': 'Static', 'group': '', 'callback': '',
 'keeparp': '', 'qosLevel': '', 'wtfast': '0', 'internetMode': 'allow', 'internetState': '1',
 'amesh_isReClient': '1', 'amesh_papMac': 'DF:29:00:5C:5B:5F'}
'''
import subprocess
import json
import sys

nmp_cache=subprocess.check_output(['ssh', '-p23232', 'super@router', 'cat', '/tmp/nmp_cache.js'], shell=False)
nmp = json.loads(nmp_cache.decode('utf-8'))

if len(sys.argv) > 1:
    get_mac=sys.argv[1]
else:
    print(nmp['maclist']) # no input, print list of MACS
    exit(0)

if len(sys.argv) > 2: # MAC + KEY
    print(nmp[get_mac][sys.argv[2]])
else:
    print(nmp[get_mac]) # Only MAC input, print all keys + values

exit(0)
