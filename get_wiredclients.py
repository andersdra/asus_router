#!/usr/bin/python3
# prints connected wired MAC's, list if anything in sys.argv
import subprocess
import json
import sys

clients=subprocess.check_output(['ssh', '-p23232', 'super@router', 'cat', '/tmp/wiredclientlist.json'])
clients = json.loads(clients.decode('utf-8'))
client_macs=clients[list(clients.keys())[0]]

if len(sys.argv) > 1:
    print(client_macs)
else:
    for mac in client_macs:
        print(mac)
