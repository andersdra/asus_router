#!/bin/bash
# get hostnames and MAC's using arp on router

router_arp="$(ssh super@router -p23232 'arp -a')"
if [ ! -e router_arp_list ];then touch list_router_arp;fi
while read -r element
do
  hostname="$(cut -d ' ' -f 1 <<< "$element")"
  mac_add="$(cut -d ' ' -f 4 <<< "$element")"
  if ! grep "$mac_add" list_router_arp &> /dev/null;then
    printf "%s;%s\n" "$hostname" "$mac_add" >> list_router_arp
  fi
done <<EOF
$router_arp
EOF
