#!/usr/bin/python3
# uses router files 'nmp_cache.js' and 'clientlist.json' to get all connected clients
# TODO add arp as another source
# HOSTNAME/VENDOR;IP;MAC;VENDOR
import subprocess
import json

subprocess.call(['./update_vendors.bash'])
nmp_cache = subprocess.check_output(['ssh', '-p23232', 'super@router', 'cat', '/tmp/nmp_cache.js'], shell=False)
nmp = json.loads(nmp_cache.decode('utf-8'))

client_list = subprocess.check_output(['ssh', '-p23232', 'super@router', 'cat', '/tmp/clientlist.json'], shell=False)
interface = json.loads(client_list.decode('utf-8'))
router_mac = list(interface.keys())[0]
networks = list(interface[router_mac].keys())
for clients in networks:
      macs = list(interface[router_mac][clients])
      client = interface[router_mac][clients]
      if len(macs) > 1:
          for mac in macs:
              mac_vendor = subprocess.check_output(['grep', mac, 'known_macs']).decode('utf-8').strip('\n').split(';')[0]
              if not client[mac]['ip']:
                  try:
                      if nmp[mac]['nickName']:
                          client_name = nmp[mac]['nickName']
                      else:
                          try:
                              client_name = nmp[mac]['name']
                          except KeyError:
                              client_name = 'NO_NAME'
                  except KeyError:
                      client_name = 'NO_NAME'
                  print("{};{};{};{}".format(client_name, 'NO_IP', mac, mac_vendor))
              else:
                  if nmp[mac]['name'] == nmp[mac]['vendor']:
                      if nmp[mac]['nickName']:
                          print("{};{};{};{}".format(nmp[mac]['nickName'], client[mac]['ip'], mac, mac_vendor))
                      else:
                          print("{};{};{};{}".format(nmp[mac]['name'], client[mac]['ip'], mac, mac_vendor))
                  else:
                      print("{};{};{};{}".format(nmp[mac]['name'], client[mac]['ip'], mac, mac_vendor))
      elif len(macs) == 1:
          try:
              mac_vendor = subprocess.check_output(['grep', macs[0], 'known_macs']).decode('utf-8').strip('\n').split(';')[0]
              print("{};{};{};{}".format(nmp[macs[0]]['nickName'], client[macs[0]]['ip'], macs[0], mac_vendor))
          except KeyError:
              print("{};{};{};{}".format(nmp[macs[0]]['name'], client[macs[0]]['ip'], macs[0], mac_vendor))


exit(0)
