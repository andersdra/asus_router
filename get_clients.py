#!/usr/bin/python3
# get IP and MAC of clients connected to most modern ASUS routers with SSH access
# use nmp_cache.js to parse name/nickNames
import subprocess
import json

client_list = subprocess.check_output(['ssh', '-p23232', 'super@router', 'cat', '/tmp/clientlist.json'], shell=False)
interface = json.loads(client_list.decode('utf-8'))
router_mac = list(interface.keys())[0]
networks = list(interface[router_mac].keys())
for clients in networks:
      macs = list(interface[router_mac][clients])
      if len(macs) > 1:
          for mac in macs:
              print("{};{}".format(interface[router_mac][clients][mac]['ip'], mac))
      elif len(macs) == 1:
          print("{};{}".format(interface[router_mac][clients][macs[0]]['ip'], macs[0]))
