#!/bin/bash
# IP;VENDOR;MAC

./update_vendors.bash
/usr/bin/python3 ./get_clients.py | \
{ while read -r mac
do
  ip="$(cut -d ';' -f1 <<< "$mac")"
  mac_adress="$(cut -d ';' -f2 <<< "$mac")"
  line="$(grep "$mac_adress" known_macs 2> /dev/null)"
  if [ "$line" ]
  then
    printf "%s;%s\n" "$ip" "$line"
  fi
done }
