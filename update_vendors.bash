#!/bin/bash
# lookup MAC address vendor for connected clients

/usr/bin/python3 ./get_clients.py | cut -d ';' -f2 | \
{ while read -r mac
do
  if ! grep "$mac" known_macs > /dev/null 2>&1
  then
    vendor="$(curl https://api.macvendors.com/"$mac" 2> /dev/null)"
    printf "%s;%s\n" "$vendor" "$mac" >> known_macs
    sleep 1
  fi
done }

exit 0
