#!/bin/bash

asus_change_username(){
ssh -p23232 super@router nvram set http_username="$1"
}

asus_get_username(){
ssh -p23232 super@router nvram get http_username
}

asus_change_passwd(){
ssh -p23232 super@router nvram set http_passwd="$1"
}

asus_get_passwd(){
ssh -p23232 super@router nvram get http_passwd
}

asus_change_ssh_key(){
ssh -p23232 super@router nvram set sshd_authkeys="$1"
}

asus_get_ssh_key(){
ssh -p23232 super@router nvram get sshd_authkeys
}

asus_router_reboot(){
ssh -p23232 super@router reboot
}
